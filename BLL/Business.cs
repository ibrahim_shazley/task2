﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL;


namespace BLL
{
    public class Business
    {
        TestDbEntities _Context;
        public Business()
        {
            _Context = new TestDbEntities();
        }

        public IEnumerable<MenuDTO> GetMenu(IEnumerable<MenuDTO> list, int? parentid)
        {
            var res = new TestDbEntities().APP_Menus.SqlQuery("exec [dbo].[sp_GetMenu]").ToList().Select(x => new MenuDTO
            {
                id = (int)x.Id,
                MenuName = x.MenuName,
                Parentid = x.ParentId,
                Levelid = x.LevelId,
                IsActive = Convert.ToBoolean(x.isActive),
                PathUrl = x.PathUrl
               // MenuList = GetMenu(list.AsQueryable(),(int)x.Id)
            });
            return res;
        }

        public IEnumerable<AdminDTO> GetAllAdmins()
        {
            return new TestDbEntities().Admin.SqlQuery("exec [dbo].[spGetAllAdmins]").ToList().Select(x => new AdminDTO
            {
                AdminId = x.Admin_Id,
                AdminName = x.Admin_Name,
            });
        }

        public IEnumerable<AdminDTO> GetAdminById(int id)
        {
            return new TestDbEntities().Admin.SqlQuery($"exec [dbo].[sp_GetAdminById] {id}").ToList().Select(x => new AdminDTO
            {
                AdminId = x.Admin_Id,
                AdminName = x.Admin_Name,
            });
        }

        public bool Addadmin(string adminName, string adminPassword)
        {
            try
            {
                new TestDbEntities().CNT.SqlQuery($"exec [dbo].[sp_AddAdmin] {adminName},{1}").ToList();
                return true;
            }
            catch (Exception ex) { return false; }
        }

        public bool DeleteAdmin(int AdminId)
        {
            try
            {
                new TestDbEntities().CNT.SqlQuery($"exec [dbo].[sp_DeleteAdmin] {AdminId}").ToList();
                return true;
            }
            catch (Exception ex)
            {
                ex.ToString();
                return false;
            }
        }
        public bool EditAdmin(string adminName, int adminid)
        {
            try
            {
                new TestDbEntities().CNT.SqlQuery($"exec [dbo].[sp_UpdateAdmin] {adminid},{adminName},{1}").ToList();
                return true;
            }
            catch (Exception ex) { return false; }
        }



    }
}
