﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL;
using Newtonsoft.Json;
using System.Web.Services;

namespace Task2
{
    public partial class EditAdmin : System.Web.UI.Page
    {
        public static Business business = new Business();

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        [WebMethod]
        public static string GetAdmin(int adminid)
        {
            return JsonConvert.SerializeObject(business.GetAdminById(adminid));
        }

        [WebMethod]
        public static bool EditAdmins(string adminName, int adminid)
        {
            return business.EditAdmin(adminName, adminid);
        }

    }
}