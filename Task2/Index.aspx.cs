﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL;
using DAL;
using System.Web.Services;
using Newtonsoft.Json;
using System.Collections;

namespace Task2
{
    public partial class Index : System.Web.UI.Page
    {
        public static Business bussiness = new Business();
        JsonSerializer serializer = new JsonSerializer();
        public static MenuDTO menu = new MenuDTO();
     
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        [WebMethod]
        public static string GetMenu()
        {
            return JsonConvert.SerializeObject(bussiness.GetMenu(menu.MenuList, menu.Parentid));
        }

    }
}