﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL;
using DAL;
using System.Web.Services;
using Newtonsoft.Json;


namespace Task2
{
   
    public partial class Admin : System.Web.UI.Page
    {
        public static Business business = new Business();
        JsonSerializer serializer = new JsonSerializer();

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        [WebMethod()]
        public static string GetAlladmins()
        {
            return JsonConvert.SerializeObject(business.GetAllAdmins());
        }

        [WebMethod]
        public static bool AddAdmin(string adminName, string adminPassword)
        {
            return business.Addadmin(adminName, adminPassword);
        }


        [WebMethod]
        public static bool DeleteAdmin(int adminid)
        {
            return business.DeleteAdmin(adminid);
        }


    }
}