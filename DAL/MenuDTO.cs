﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
   public class MenuDTO
    {
        public int id { get; set; }
        public string MenuName { get; set; }
        public int? Parentid { get; set; }
        public int Levelid { get; set; }
        public bool IsActive { get; set; }
        public string PathUrl { get; set; }
        public IEnumerable<MenuDTO> MenuList { get; set; }



    }
}
